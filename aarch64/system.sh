#!/bin/bash

# docker server
if type dgx-docker-cleanup.sh >/dev/null 2>&1; then
    echo Skip $0
    exit 0
fi

DIR=$(dirname $0|sed -e "s|^\.[^\.]|$PWD|")
if [ ! -f /usr/bin/sudo ]; then
    cp $DIR/sudo /usr/bin/sudo
    chmod +x /usr/bin/sudo
fi

RELEASE=$(egrep ^DISTRIB_CODENAME= /etc/lsb-release|awk -F= '{print $2}')

# update db
sudo apt-get update || exit 1

# docker
if [ -f /.dockerenv ]; then
    echo Skip $0
    exit 0
fi

# python dependencies
sudo apt-get install -y \
     libreadline-dev \
     zlib1g-dev \
     libbz2-dev \
     libsqlite3-dev \
     libssl-dev \
     libffi-dev \
     || exit 1

# node
sudo apt-get install -y nodejs || exit 1

# magenta
sudo apt-get install -y timidity || exit 1
sudo apt-get install -y vmpk || exit 1
sudo apt-get install -y fluidsynth || exit 1
sudo apt-get install -y build-essential || exit 1
sudo apt-get install -y libasound2-dev || exit 1
sudo apt-get install -y pavucontrol || exit 1

# TensorFlow
sudo apt-get install -y libhdf5-serial-dev hdf5-tools || exit 1
sudo apt-get install -y libopenblas-base || exit 1

# OpenCV3
sudo apt-get install -y libcanberra-gtk-module || exit 1

# scipy
sudo apt-get install -y gcc gfortran g++ || exit 1
sudo apt-get install -y libblas-dev libatlas-base-dev || exit 1
sudo apt-get install -y liblapack-dev || exit 1
sudo apt-get install -y libopenblas-dev || exit 1

# scikit-image
sudo apt-get install -y libjpeg-dev || exit 1

# mask-r-cnn
sudo apt-get install -y libfreetype6-dev || exit 1
sudo apt-get install -y libgeos-dev || exit 1

# object_detection
sudo apt-get install -y protobuf-compiler || exit 1

# camera
sudo apt-get install -y uvcdynctrl || exit 1

# openpose
sudo apt-get install -y swig liblapacke-dev || exit 1

# atari
sudo apt-get install -y swig liblapacke-dev || exit 1
sudo apt-get install -y curl ffmpeg cmake || exit 1
sudo apt-get install -y libavcodec-extra || exit 1
sudo apt-get install -y ffmpeg || exit 1

# pyglet
sudo apt-get install -y mercurial || exit 1

# pydot
sudo apt-get install -y graphviz || exit 1

# EV3dc
sudo apt-get install -y python3-tk || exit 1

# optuna(mysql)
sudo apt-get install -y mysql-client || exit 1
sudo apt-get install -y default-libmysqlclient-dev || exit 1

# Xavier or edgetpu workaround
# pip3
sudo apt-get install -y python3-pip || exit 1
# python
if [ -f /sys/firmware/devicetree/base/model ] && grep -q "MX8MQ" /sys/firmware/devicetree/base/model; then
    sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.5 1 || exit 1
    sudo update-alternatives --set python /usr/bin/python3.5 || exit 1
else
    sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1 || exit 1
    sudo update-alternatives --set python /usr/bin/python3.6 || exit 1
fi
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2.7 2 || exit 1
# venv
sudo apt-get install -y python3-venv || exit 1
# pip
sudo update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1 || exit 1
sudo update-alternatives --set pip /usr/bin/pip3 || exit 1

# python libraries
sudo apt-get install -y python3-scipy || exit 1
sudo apt-get install -y python3-skimage || exit 1
sudo apt-get install -y python3-sklearn || exit 1
# magenta
#sudo apt-get install -y python3-numba || exit 1
sudo apt-get install -y python3-numexpr || exit 1
#sudo apt-get install -y python3-llvmlite || exit 1
#sudo apt-get install -y llvm-6.0 llvm-6.0-dev || exit 1
sudo apt-get install -y llvm-7 llvm-7-dev || exit 1

# clean
sudo apt-get autoremove -y --purge || exit 1

echo OK $0
