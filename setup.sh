#!/bin/bash

eval "$(pyenv init - --no-rehash)"
eval "$(pyenv virtualenv-init -)"

DIR=$(dirname $0|sed -e "s|^\.$|$PWD|")
(cd $DIR && git pull)
source $DIR/versions.sh

REPO=$(basename $PWD)
echo $REPO
#python3 --version
#pip3 --version
#pyenv --version

if [ -f /sys/firmware/devicetree/base/model ] && grep -q "MX8MQ" /sys/firmware/devicetree/base/model; then
    PY=system
    SYS='--system-site-packages'
elif [ x$(arch) == x"aarch64" ]; then
    PY=system
    SYS='--system-site-packages'
elif [ -f .python2 ]; then
    PY=$PYTHON2
    SYS=''
else
    PY=$PYTHON3
    SYS=''
fi

if [ -d ~/.pyenv -a ! ~/.pyenv/plugins/pyenv-update ]; then
    git clone git://github.com/yyuu/pyenv-update.git ~/.pyenv/plugins/pyenv-update
fi
if [ -d ~/.pyenv/plugins/pyenv-update ]; then
    pyenv update
fi

echo pyenv virtualenv $SYS -f $PY $REPO
pyenv virtualenv $SYS -f $PY $REPO
pyenv local $REPO
pyenv activate $REPO

if [ -f .python2 ]; then
    python2 -m pip install -U pip setuptools wheel
else
    python3 -m pip install -U pip setuptools wheel
    hash -r
    if [ x$(arch) == x"i386" -o x$(arch) == x"arm64" ]; then
	#pip install cython
	#pip install PyQt5
	mkdir -p ~/.matplotlib
	echo 'backend : Qt5Agg' > ~/.matplotlib/matplotlibrc
	#echo 'font.family: IPAexGothic' >> ~/.matplotlib/matplotlibrc
    fi
fi

echo OK $0
