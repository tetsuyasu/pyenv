#!/bin/bash

function brew_install () {
    _package=$*
    brew install $_package || brew reinstall $_package || exit 1
}

function brew_cask_install () {
    _package=$*
    brew cask install $_package || brew cask reinstall $_package || exit 1
}

# EV3
#brew_install libusb

# baselines
#brew_install mpich
if [ -d /usr/local/Cellar/mpich ]; then
    brew uninstall mpich
    brew unlink mpich
fi

# build
brew_install curl
brew_install cmake

# remove node
if [ -d /usr/local/Cellar/node ]; then
    brew unlink --force node
    brew uninstall --ignore-dependencies --force node
fi
brew cleanup node

# pyenv
brew install zlib
brew install readline
brew install xz

# face
brew_install fdupes

# magenta
#brew_install timidity
brew_install fluidsynth
#brew_cask_install vmpk
brew_install libao
brew_install libvorbis

# atari
#brew_install libav
brew_install ffmpeg
brew_install imagemagick
brew_install boost sdl2 swig wget

# stable_baselines
brew_install openmpi

# matplotlib
brew_install tcl-tk
#brew_install pyqt
#brew_install pygtk
#brew_install wxpython
brew_install libjpeg

# optuna(mysql)
brew_install mysql

# M1
brew install openblas
brew install xz

echo OK $0
