#!/bin/bash

DIR=$(dirname $0|sed -e "s|^\.$|$PWD|")
(cd $DIR && git pull)
source $DIR/versions.sh

while getopts :2h OPT; do
    case $OPT in
        2)
            PYTHON2=$OPTARG
            ;;
        h)
            echo 'Usage: pyenv.sh [-a] [-h]'
	    echo '  -a: install all python versions (2 and 3)'
            exit 1
            ;;
    esac
done

if [ x"$PYENV_ROOT" == x ]; then
    export PYENV_ROOT=$HOME/.pyenv
    export PATH=$PYENV_ROOT/bin:$PATH
fi

if [ $(arch) == i386 ] || [ $(arch) == arm64 ]; then # Mac
    # pyenv
    brew update -v
    brew install pyenv
    brew install pyenv-virtualenv
else
    # pyenv
    if [ ! -d $PYENV_ROOT ]; then
	git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT
    fi
    if [ ! -d $PYENV_ROOT/plugins/pyenv-virtualenv ]; then
	git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_ROOT/plugins/pyenv-virtualenv
    fi
fi

if type pyenv >/dev/null 2>&1; then
    eval "$(pyenv init - --no-rehash)"
    eval "$(pyenv virtualenv-init -)"
else
    echo pyenv not found, exit.
    exit 1
fi

if type apt-get >/dev/null 2>&1; then
    if [ ! -d $PYENV_ROOT/plugins/pyenv-update ]; then
	git clone git://github.com/pyenv/pyenv-update.git $PYENV_ROOT/plugins/pyenv-update
    fi
    pyenv update
elif type brew >/dev/null 2>&1; then
    brew upgrade pyenv
else
    echo not supported
    exit 1
fi

# cleaning
#for i in $(pyenv versions|sed -e 's/\*//g'|sed -E -e 's/\(.*\)//g'|sed -e 's/ //g'|grep -v "^system$"|grep '^[0-9]'|grep -v "^$PYTHON2$"|grep -v "^$PYTHON3$"|grep -v "^$PYTHON2/"|grep -v "^$PYTHON3/"); do
#    echo Uninstalling $i
#    pyenv uninstall -f $i
#done

# docker
if [ -f /.dockerenv ]; then
    unset PYTHON2
fi

echo installing $PYTHON3

if [ $(arch) == i386 ] || [ $(arch) == arm64 ]; then # Mac
    # https://qiita.com/itoru257/items/c52cab383eaae4a242d4
    export CFLAGS="-I$(xcrun --show-sdk-path)/usr/include"
    export CPPFLAGS="-I$(xcrun --show-sdk-path)/usr/include"
    export LDFLAGS="-L$(brew --prefix zlib)/lib"
    for i in xz boost-python3 wxmac qt openblas mysql lapack pybind11 libtiff protobuf openssl@1.1 readline bzip2 zlib; do
	export CFLAGS="-I$(brew --prefix $i)/include $CFLAGS"
	export CPPFLAGS="-I$(brew --prefix $i)/include $CPPFLAGS"
	export LDFLAGS="-L$(brew --prefix $i)/lib $LDFLAGS"
    done   
    export OPENBLAS="$(brew --prefix openblas)/lib"
    export PYTHON_CONFIGURE_OPTS=--enable-unicode=ucs2
else
    export PYTHON_CONFIGURE_OPTS="--enable-shared"
fi

if [[ x$(arch) == x"arm64" ]] && [ -f /opt/homebrew/bin/brew ]; then # Mac M1
    case $PYTHON3 in
	3.8.*)
	    # https://qiita.com/ketaro-m/items/ebae35c49d55aa86dfcf
	    pyenv install --patch $PYTHON3 < $DIR/i386/3.8.3.patch || exit 1
	    ;;
	*)
	    pyenv install -s -v $PYTHON3 || exit 1
	    ;;
    esac
else
    pyenv install -s -v $PYTHON3 || exit 1
fi

if [ x"$(arch)" == x"x86_64" ] || [ x"$(arch)" == x"i386" ] && [ x"$PYTHON2" != x ]; then
    echo installing $PYTHON2
    PYTHON_CONFIGURE_OPTS="--enable-shared" CONFIGURE_OPTS="$OPTS" pyenv install -s $PYTHON2
fi

pyenv global system

echo OK $0
