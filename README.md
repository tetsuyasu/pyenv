# pyenv


## clone
```
$ mkdir ~/git
$ cd ~/git
$ git clone https://gitlab.com/tetsuyasu/pyenv.git/
```


## install
```
$ cd ~/git/pyenv
$ cat bashrc >> ~/.bashrc
$ source ~/.bashrc
$ ./install.sh
```


## usage
```
$ cd ~/git
$ git clone https://gitlab.com/tetsuyasu/dlwpn.git/
$ cd dlwpn
$ ../pyenv/setup.sh
$ ./setup.sh
```
