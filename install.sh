#!/bin/bash

set -xe

DIR=$(dirname $0|sed -e "s|^\.$|$PWD|")
(cd $DIR && git pull)
source $DIR/versions.sh

$DIR/system.sh
$DIR/pyenv.sh

echo OK $0
