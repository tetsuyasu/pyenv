# pyenv

## to do
1. venvの対応
- https://soymsk.hatenablog.com/entry/2017/04/03/022458
- pyenvもvenvもdirenvで切り替えたい。
- install.shやsetup.shは名称そのままで基本的にvenvで動くようにする。
- direnv
  - https://github.com/direnv/direnv/blob/master/docs/hook.md

1. tensorflow>=1.15の対応
  - <1.14
    - tensorflow-gpu: GPU
    - tensorflow: CPU
  - <=1.15
    - tensorflow: GPU/CPU
    - (tensorflow-cpu: CPU)
  - <1.14でGPUのあるときだけ-gpuを付ける。

1. 構造解析(旧)
  - install.sh -> system.sh -> install pyenv req
    	       	  	    -> install pyenv
			    -> install misc
               -> pyenv init
	       -> pyenv.sh

1. 新構造
  - install.sh -> system.sh -> install misc
	       -> install_pyenv.sh -> install pyenv req
    	       	  	    	   -> install pyenv
               -> pyenv init
	       -> pyenv.sh


## usage
install.sh: install system and global environment
  -> system.sh: install system environment
  -> node.sh: install global nodejs environment

setup.sh: create virtualenv

tensorflow.sh: install tensorflow

opencv-python: copy cv2.so


## Xavier対応方針
systemを元にして仮想環境を作る。--system-site-packagesを指定する。
Xavierのときは、
  system.shでsystemライブラリをapt-getでインストールする。
  setup.shでsystemライブラリをpipでインストールしない。
