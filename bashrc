#!/bin/bash

# lang
for i in $(env|egrep ^LC_|awk -F= '{print $1}'); do unset $i; done
export LC_CTYPE=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGER=en
export LESSCHARSET=utf-8
export PYTHONIOENCODING=utf-8

# pyenv
export PYENV_ROOT=$HOME/.pyenv
export PATH=$PYENV_ROOT/bin:$PATH

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"


# nodebrew
if [ -d /usr/local/opt/node@6/bin ]; then
    export PATH="/usr/local/opt/node@6/bin:$PATH"
elif [ -d $HOME/.nodebrew/current ]; then
    export PATH=$PATH:$HOME/.nodebrew/current/bin
    export NODE_PATH=$HOME/.nodebrew/current/lib/node_modules
fi

# clean path
export PATH=`echo $PATH|tr ':' '\n'|awk '!a[$0]++'|paste -d: -s -`

# EOF
