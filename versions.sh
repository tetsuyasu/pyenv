#!/bin/bash

set -xe

# (1) nvidiaのTensorFLowイメージの最新版のTensorFlowやPythonバージョンを基準とする。
# https://docs.nvidia.com/deeplearning/dgx/tensorflow-release-notes/
# https://docs.nvidia.com/deeplearning/frameworks/support-matrix/index.html

# (2) tensorflow-macos
# https://pypi.org/project/tensorflow-macos/#files

DIR=$(dirname $0|sed -e "s|^\.$|$PWD|")

# Python
# (pyenv install --listから選ぶ)
unset PYTHON2 # not needed
if [ -f /proc/cpuinfo ] && [ x"$(grep ^flags /proc/cpuinfo|grep avx)" == x ]; then # no avx
    PYTHON3=3.7.11 # for tensorflow-no-avx
elif [[ x$(arch) == x"arm64" ]]; then # Mac M1
    PYTHON3=3.9.6 # tensorflow-macos
elif [[ x$(arch) == x"i386" ]]; then # Mac
    PYTHON3=3.8.11 # tensorflow-macos
else # Linux
    PYTHON3=3.8.11 # same as nvidia docker image
fi

REQPY=$(echo $PYTHON3|awk -F. '{print $1"."$2}')
SYSPY=$(python3 -V|awk '{print $2}'|awk -F. '{print $1"."$2}')

echo REQPY=$REQPY
echo SYSPY=$SYSPY

# override with local settings
if [ "$PWD" != "$DIR" -a -f versions.sh ]; then
    source versions.sh
fi
