#!/bin/bash

set -xe

DIR=$(dirname $0|sed -e "s|^\.[^\.]|$PWD|")

if type apt-get >/dev/null 2>&1; then
    sudo apt-get install -y python3-pip python3-venv direnv
elif type brew >/dev/null 2>&1; then
    brew install direnv
else
    echo not supported, exit.
    exit 1
fi

rm -rf .pyenv .python-version

repo=$(basename $PWD)
python3 -m venv .venv/$repo

cat > .envrc <<EOF
repo=$(basename $PWD)
if [ -f .venv/$repo/bin/activate ]; then
   source .venv/$repo/bin/activate
   unset PS1
fi
EOF

direnv allow

echo OK $0
