#!/bin/bash

# docker server
if type dgx-docker-cleanup.sh >/dev/null 2>&1; then
    echo Skip $0
    exit 0
fi

DIR=$(dirname $0|sed -e "s|^\.[^\.]|$PWD|")
if [ ! -f /usr/bin/sudo ]; then
    cp $DIR/sudo /usr/bin/sudo
    chmod +x /usr/bin/sudo
fi

RELEASE=$(egrep ^DISTRIB_CODENAME= /etc/lsb-release|awk -F= '{print $2}')

# update db
sudo apt-get update || exit 1

# docker
#if [ -f /.dockerenv ]; then
#    echo Skip $0
#    exit 0
#fi

# python dependencies
if [ x"$RELEASE" != xbionic ]; then
    sudo apt-get install -y \
	 libreadline6 \
	 libreadline6-dev \
	|| exit 1
else
    sudo apt-get install -y \
	 libreadline-dev \
	|| exit 1
fi
sudo apt-get install -y \
     zlib1g-dev \
     libbz2-dev \
     libsqlite3-dev \
     libssl-dev \
     libffi-dev \
     tk-dev \
     liblzma-dev \
     libsm6 \
     libxext6 \
     libxrender-dev \
     libcairo2 \
     graphviz \
     liblzma-dev \
     || exit 1

# python3.6
if [ x"$RELEASE" != xbionic ]; then
    sudo apt-get install -y software-properties-common || exit 1
    echo | sudo add-apt-repository ppa:deadsnakes/ppa || exit 1
    sudo apt-get update || exit 1
    sudo apt-get install -y python3.6 || exit 1
fi

# node
sudo apt-get install -y nodejs || exit 1

# TensorFlow
sudo apt-get install -y libopenblas-base || exit 1

# OpenCV3
sudo apt-get install -y libcanberra-gtk-module || exit 1

# scipy
sudo apt-get install -y gcc gfortran g++ || exit 1
sudo apt-get install -y libblas-dev libatlas-base-dev || exit 1
sudo apt-get install -y liblapack-dev || exit 1
sudo apt-get install -y libopenblas-dev || exit 1

# scikit-image
sudo apt-get install -y libjpeg-dev || exit 1

# mask-r-cnn
sudo apt-get install -y libfreetype6-dev libhdf5-dev || exit 1
sudo apt-get install -y libhdf5-dev || exit 1

# object_detection
sudo apt-get install -y protobuf-compiler || exit 1

# camera
sudo apt-get install -y uvcdynctrl || exit 1

# openpose
sudo apt-get install -y swig liblapacke-dev || exit 1

# atari
sudo apt-get install -y swig liblapacke-dev || exit 1
sudo apt-get install -y curl ffmpeg cmake || exit 1
sudo apt-get install -y libavcodec-extra || exit 1
if [ x"$RELEASE" == xbionic ]; then
    sudo apt-get install -y ffmpeg || exit 1
else
    sudo apt-get install -y libav-tools || exit 1
fi

# pyglet
sudo apt-get install -y mercurial || exit 1

# pydot
sudo apt-get install -y graphviz || exit 1

# EV3dc
sudo apt-get install -y python3-tk || exit 1

# optuna(mysql)
sudo apt-get install -y mysql-client || exit 1
sudo apt-get install -y libmysqlclient-dev || exit 1

# IPA gothic
sudo apt-get install -y fonts-ipafont-gothic fonts-ipaexfont-gothic || exit 1
/bin/rm -rf ~/.cache/matplotlib

# xvfb
sudo apt-get install -y xvfb python-opengl || exit 1
#!/bin/bash

# docker server
if type dgx-docker-cleanup.sh >/dev/null 2>&1; then
    echo Skip $0
    exit 0
fi

if [ ! -f /usr/bin/sudo ]; then
    cp $DIR/sudo /usr/bin/sudo
    chmod +x /usr/bin/sudo
fi

RELEASE=$(egrep ^DISTRIB_CODENAME= /etc/lsb-release|awk -F= '{print $2}')

# update db
sudo apt-get update || exit 1

# docker
if [ -f /.dockerenv ]; then
    echo Skip $0
    exit 0
fi

# python dependencies
if [ x"$RELEASE" != xbionic ]; then
    sudo apt-get install -y \
	 libreadline6 \
	 libreadline6-dev \
	|| exit 1
else
    sudo apt-get install -y \
	 libreadline-dev \
	|| exit 1
fi
sudo apt-get install -y \
     build-essential \
     zlib1g-dev \
     libbz2-dev \
     libsqlite3-dev \
     libssl-dev \
     libffi-dev \
     tk-dev \
     || exit 1

# python3.6
if [ x"$RELEASE" != xbionic ]; then
    sudo apt-get install -y software-properties-common || exit 1
    echo | sudo add-apt-repository ppa:deadsnakes/ppa || exit 1
    sudo apt-get update || exit 1
    sudo apt-get install -y python3.6 || exit 1
fi

# node
sudo apt-get install -y nodejs || exit 1

# magenta
#sudo apt-get install -y timidity || exit 1
#sudo apt-get install -y vmpk || exit 1
#sudo apt-get install -y fluidsynth || exit 1
#sudo apt-get install -y libasound2-dev || exit 1
#sudo apt-get install -y pavucontrol || exit 1

# TensorFlow
sudo apt-get install -y libopenblas-base || exit 1

# OpenCV3
sudo apt-get install -y libcanberra-gtk-module || exit 1

# scipy
sudo apt-get install -y gcc gfortran g++ || exit 1
sudo apt-get install -y libblas-dev libatlas-base-dev || exit 1
sudo apt-get install -y liblapack-dev || exit 1
sudo apt-get install -y libopenblas-dev || exit 1

# scikit-image
sudo apt-get install -y libjpeg-dev || exit 1

# mask-r-cnn
sudo apt-get install -y libfreetype6-dev libhdf5-dev || exit 1
sudo apt-get install -y libhdf5-dev || exit 1

# object_detection
sudo apt-get install -y protobuf-compiler || exit 1

# camera
sudo apt-get install -y uvcdynctrl || exit 1

# openpose
sudo apt-get install -y swig liblapacke-dev || exit 1

# atari
sudo apt-get install -y swig liblapacke-dev || exit 1
sudo apt-get install -y curl ffmpeg cmake || exit 1
sudo apt-get install -y libavcodec-extra || exit 1
if [ x"$RELEASE" == xbionic ]; then
    sudo apt-get install -y ffmpeg || exit 1
else
    sudo apt-get install -y libav-tools || exit 1
fi

# pyglet
sudo apt-get install -y mercurial || exit 1

# pydot
sudo apt-get install -y graphviz || exit 1

# EV3dc
sudo apt-get install -y python3-tk || exit 1

# optuna(mysql)
sudo apt-get install -y mysql-client || exit 1
sudo apt-get install -y libmysqlclient-dev || exit 1

# IPA gothic
sudo apt-get install -y fonts-ipafont-gothic fonts-ipaexfont-gothic || exit 1
/bin/rm -rf ~/.cache/matplotlib

# xvfb
sudo apt-get install -y xvfb python-opengl || exit 1

# pandas 0.25.0 workaround
sudo apt-get install -y liblzma-dev || exit 1

# graphviz
sudo apt-get install -y graphviz || exit 1

# clean
sudo apt-get autoremove -y --purge || exit 1

echo OK $0
